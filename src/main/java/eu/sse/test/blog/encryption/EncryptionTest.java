package eu.sse.test.blog.encryption;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;
//import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Example to encrypt and decrypt using a random key, password and encrypting
 * with RSA key
 *
 * for the symmetric encryption we well use AES/CBC/PKCS5Padding
 *
 * Cipher javadoc:
 * https://docs.oracle.com/javase/7/docs/api/javax/crypto/Cipher.html
 *
 * we will use basic Java 8 tools. For more options, use
 * https://www.bouncycastle.org
 *
 * <pre>
 * keytool -genkeypair -keystore example.jks -alias test -keyalg RSA
 *   -keysize 1024 -dname "CN=test"
 *   -validity 3650 -storepass changeit
 * </pre>
 *
 * symmetric encryption result: iv | ciphertext | tag
 *
 * where tag is hmac ( ciphertext )
 *
 * we will use encrypt and mac
 *
 * @author Gabriel
 */
public class EncryptionTest {

    private static final Logger LOGGER = Logger.getLogger(EncryptionTest.class.getName());

    private static final String PLAINTEXT = "Lorem ipsum dolor sit amet, "
            + "consectetur adipiscing elit. Vestibulum rhoncus magna at felis "
            + "feugiat viverra. Nullam varius, nibh sit amet tempor commodo, "
            + "diam diam mollis tellus, in pretium lectus ante nec mauris.";

    // parameters for AES-128 with block size 128 bit
    private static final String SYMMETRIC_CIPHER_NAME = "AES/CBC/PKCS5Padding";
    private static final String SYMMETRIC_KEY_ALG = "AES";
    private static final int SYMMETRIC_BLOCK_SIZE = 128;
    //  key size for different ciphers
    private static final byte[] SYMMETRIC_KEY = Base64.getDecoder().decode("4Y6kfyPmosh1I6dSkMEk1Q==");
    private static final String TEXT_PASSWORD = "aFpZqRW`]3c\"V&8TV{D4Rq<R";
    private static final String PBKDF_ALG = "PBKDF2WithHmacSHA256";
    private static final int PBKDF_INTERATIONS = 800000;
    private static final String HASH_ALGORITHM_NAME = "HmacSHA256";
    private static final String PBE_CIPHER_NAME = "PBEWithHmacSHA256AndAES_128";
    
    private static final String KEYSTORE_FILE = "config/example.jks";
    private static final String KEYSTORE_FILE_PASSWORD = "changeit";
    private static final String KEYSTORE_KEY_PASSWORD = "changeit";
    private static final String KEYSTORE_ALIAS = "test";
    private static final String PKI_CIPHER_ALG = "RSA/ECB/PKCS1Padding";
    // available in Java 8
//    private static final String PKI_CIPHER_ALG = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";
    
    public static void main(String[] args) {
        // uncomment to list services provided by BouncyCastle
//        Security.addProvider(new BouncyCastleProvider());        
        EncryptionTest test = new EncryptionTest();
        test.printAvailableServices();

        try {
            
            EncryptionTestParameters encryptionParams = null;
            
            // direct symmetric encryption
            encryptionParams = new EncryptionTestParameters();
            encryptionParams.setPlaintext(PLAINTEXT.getBytes("UTF-8"));
            encryptionParams.setKey(SYMMETRIC_KEY);
            encryptionParams = test.symmetricEncryption(encryptionParams);
            test.symmetricDecryption(encryptionParams);
            
            // password encryption
            encryptionParams = new EncryptionTestParameters();
            encryptionParams.setPlaintext(PLAINTEXT.getBytes("UTF-8"));
            encryptionParams = test.symmetricPasswordEncryption(encryptionParams, TEXT_PASSWORD);
            test.symmetricPasswordDecryption(encryptionParams, TEXT_PASSWORD);
            
            // password encryption with direct PBE cipher
            encryptionParams = new EncryptionTestParameters();
            encryptionParams.setPlaintext(PLAINTEXT.getBytes("UTF-8"));
            encryptionParams = test.symmetricPasswordEncryption2(encryptionParams, TEXT_PASSWORD);
            test.symmetricPasswordDecryption2(encryptionParams, TEXT_PASSWORD);
            
            // rsa encryption
            encryptionParams = new EncryptionTestParameters();
            encryptionParams.setPlaintext(PLAINTEXT.getBytes("UTF-8"));
            test.asymmetricEncryption(encryptionParams);
            test.asymmetricDecryption(encryptionParams);
            
        } catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "main", ex);
        }

    }

    private void printAvailableServices() {
        Provider[] providerArray = Security.getProviders();
        for (Provider provider : providerArray) {
            System.out.println(provider.getName());
            for (Provider.Service service : provider.getServices()) {
                System.out.print(service.getType());
                System.out.print(" ");
                System.out.println(service.getAlgorithm());
            }
            System.out.println("---");
        }
    }

    /**
     * encrypt the plaintext
     * assumes plaintext and key filled
     * 
     * the encryption will fill MAC (message authentication code) and IV
     *
     * @return
     */
    private EncryptionTestParameters symmetricEncryption(EncryptionTestParameters encryptionParams) throws
             NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {

        LOGGER.log(Level.INFO, "symmetric encryption, plaintext: {0}", new String(encryptionParams.getPlaintext(),"UTF-8"));

        SecureRandom rnd = new SecureRandom();
        byte[] iv = new byte[SYMMETRIC_BLOCK_SIZE / 8];
        rnd.nextBytes(iv);
        encryptionParams.setIv(iv);
        LOGGER.log(Level.INFO, "iv: {0}", Base64.getEncoder().encodeToString(iv));
        IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
        SecretKey symmetricKey = new SecretKeySpec(encryptionParams.getKey(), SYMMETRIC_KEY_ALG);

        Cipher cipher = Cipher.getInstance(SYMMETRIC_CIPHER_NAME);
        cipher.init(Cipher.ENCRYPT_MODE, symmetricKey, ivParamSpec);

        // for HMAC we should be able to use the same key as for encryption
        // for CBC-MAC it may not be the case
        // https://en.wikipedia.org/wiki/CBC-MAC#Using_the_same_key_for_encryption_and_authentication
        Mac mac = Mac.getInstance(EncryptionTest.HASH_ALGORITHM_NAME);
        mac.init(symmetricKey);

        byte[] encrypted = cipher.doFinal(encryptionParams.getPlaintext());
        encryptionParams.setCiphertext(encrypted);
        byte[] authTag = mac.doFinal(encrypted);
        encryptionParams.setMac(authTag);

        LOGGER.log(Level.INFO, "Input encrypted, tag value: {0}, ciphertext: {1}",
                new Object[]{
                    Base64.getEncoder().encodeToString(authTag),
                    Base64.getEncoder().encodeToString(encrypted)
                });
        return encryptionParams;
        
    }

    /**
     * decrypt the output file
     *
     * @param prefixLength bytes to skip, used to store password salt
     * @param key
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     */
    private void symmetricDecryption(EncryptionTestParameters encryptionParams) throws
             NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {

        IvParameterSpec ivParamSpec = new IvParameterSpec(encryptionParams.getIv());
        SecretKey symmetricKey = new SecretKeySpec(encryptionParams.getKey(), SYMMETRIC_KEY_ALG);

        Cipher cipher = Cipher.getInstance(SYMMETRIC_CIPHER_NAME);
        cipher.init(Cipher.DECRYPT_MODE, symmetricKey, ivParamSpec);

        Mac mac = Mac.getInstance(EncryptionTest.HASH_ALGORITHM_NAME);
        mac.init(symmetricKey);
        
        byte[] computedMac = mac.doFinal(encryptionParams.getCiphertext());
        LOGGER.log(Level.INFO, "computed mac: {0}", Base64.getEncoder().encodeToString(computedMac));
        
        if(this.compareTags(computedMac, encryptionParams.getMac())) {
            byte[] decrypted = cipher.doFinal(encryptionParams.getCiphertext());
            LOGGER.log(Level.INFO, "decrypted text: {0}", new String(decrypted,"UTF-8"));
            
        } else {
            LOGGER.log(Level.WARNING, "Decryption failed, authentication tag doesn't match");
            throw new IllegalArgumentException("authentication failed");
        }
        
    }
    
    private EncryptionTestParameters symmetricPasswordEncryption(EncryptionTestParameters params, String password) throws Exception {
        
        // create salt
        SecureRandom rnd = new SecureRandom();
        // salt can have arbitrary length
        byte[] psswdSalt = new byte[SYMMETRIC_BLOCK_SIZE]; 
        rnd.nextBytes(psswdSalt);
        params.setSalt(psswdSalt);
        
        // create key from password
        SecretKeyFactory secKeyFactory = SecretKeyFactory.getInstance(PBKDF_ALG);
        KeySpec pbeSpec = new PBEKeySpec(password.toCharArray(), psswdSalt, PBKDF_INTERATIONS, SYMMETRIC_KEY.length*8);
        SecretKey pbeSecretKey = secKeyFactory.generateSecret(pbeSpec);
        SecretKey secKey = new SecretKeySpec(pbeSecretKey.getEncoded(), SYMMETRIC_KEY_ALG);
        params.setKey(secKey.getEncoded());
        this.symmetricEncryption(params);
        return params;
    }
    
    private EncryptionTestParameters symmetricPasswordDecryption(EncryptionTestParameters params, String password) throws Exception {

        // create key from password
        SecretKeyFactory secKeyFactory = SecretKeyFactory.getInstance(PBKDF_ALG);
        KeySpec pbeSpec = new PBEKeySpec(password.toCharArray(), params.getSalt(), PBKDF_INTERATIONS, SYMMETRIC_KEY.length*8);
        SecretKey pbeSecretKey = secKeyFactory.generateSecret(pbeSpec);
        SecretKey secKey = new SecretKeySpec(pbeSecretKey.getEncoded(), SYMMETRIC_KEY_ALG);
        params.setKey(secKey.getEncoded());
        this.symmetricDecryption(params);
        return params;
    }
    
   
    private EncryptionTestParameters symmetricPasswordEncryption2(EncryptionTestParameters params, String password) throws Exception {
              
        SecureRandom rnd = new SecureRandom();
              
        // create salt,  salt can have arbitrary length
        byte[] psswdSalt = new byte[SYMMETRIC_BLOCK_SIZE]; 
        rnd.nextBytes(psswdSalt);
        params.setSalt(psswdSalt);
        
        byte[] iv = new byte[SYMMETRIC_BLOCK_SIZE / 8];
        rnd.nextBytes(iv);
        params.setIv(iv);        
        IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
        
        PBEParameterSpec pbeParamSpec = new PBEParameterSpec(psswdSalt, PBKDF_INTERATIONS, ivParamSpec);
        PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray());
        SecretKeyFactory pbeKeyFactory = SecretKeyFactory.getInstance(PBE_CIPHER_NAME);
        SecretKey pbeKey = pbeKeyFactory.generateSecret(pbeKeySpec);
        
        Cipher cipher = Cipher.getInstance(PBE_CIPHER_NAME);
        cipher.init(Cipher.ENCRYPT_MODE, pbeKey, pbeParamSpec);

        // for HMAC we shuld be able to use the same key as for encryption
        // for CBC-MAC it may not be the case
        // https://en.wikipedia.org/wiki/CBC-MAC#Using_the_same_key_for_encryption_and_authentication
        Mac mac = Mac.getInstance(EncryptionTest.HASH_ALGORITHM_NAME);
        mac.init(pbeKey);

        byte[] encrypted = cipher.doFinal(params.getPlaintext());
        params.setCiphertext(encrypted);
        byte[] authTag = mac.doFinal(encrypted);
        params.setMac(authTag);

        LOGGER.log(Level.INFO, "Input encrypted, tag value: {0}, ciphertext: {1}",
                new Object[]{
                    Base64.getEncoder().encodeToString(authTag),
                    Base64.getEncoder().encodeToString(encrypted)
                });
        return params;
        
    }
    
    private EncryptionTestParameters symmetricPasswordDecryption2(EncryptionTestParameters encryptionParams, String password) throws Exception {

 
        // create salt,  salt can have arbitrary length
        byte[] psswdSalt = encryptionParams.getSalt(); 
        byte[] iv = encryptionParams.getIv();
        
        IvParameterSpec ivParamSpec = new IvParameterSpec(iv);
        PBEParameterSpec pbeParamSpec = new PBEParameterSpec(psswdSalt, PBKDF_INTERATIONS, ivParamSpec);
        PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray());
        SecretKeyFactory pbeKeyFactory = SecretKeyFactory.getInstance(PBE_CIPHER_NAME);
        SecretKey pbeKey = pbeKeyFactory.generateSecret(pbeKeySpec);
        
        
        Cipher cipher = Cipher.getInstance(PBE_CIPHER_NAME);
        cipher.init(Cipher.DECRYPT_MODE, pbeKey, pbeParamSpec);

        Mac mac = Mac.getInstance(EncryptionTest.HASH_ALGORITHM_NAME);
        mac.init(pbeKey);
        
        byte[] computedMac = mac.doFinal(encryptionParams.getCiphertext());
        LOGGER.log(Level.INFO, "computed mac: {0}", Base64.getEncoder().encodeToString(computedMac));
        
        if(this.compareTags(computedMac, encryptionParams.getMac())) {
            byte[] decrypted = cipher.doFinal(encryptionParams.getCiphertext());
            LOGGER.log(Level.INFO, "decrypted text: {0}", new String(decrypted,"UTF-8"));
            
        } else {
            LOGGER.log(Level.WARNING, "Decryption failed, authentication tag doesn't match");
            throw new IllegalArgumentException("authentication failed");
        }
        return encryptionParams;
    }    
    
    private EncryptionTestParameters asymmetricEncryption(EncryptionTestParameters params) throws Exception {
        // generate random key without revealing the real key bytes
        // some HSM (hardware security modules) won't allow to set or get bytes
        // revealing the real key value
        KeyGenerator keyGenerator = KeyGenerator.getInstance(SYMMETRIC_KEY_ALG);
        SecretKey symmetricKey = keyGenerator.generateKey();
         
        // this assumes there's whole keypair (including private key)
        // normally only a certificate is available
        KeyStore.PrivateKeyEntry privKeyEntry = (KeyStore.PrivateKeyEntry) this.getKeystoreEntry();
        PublicKey pubKey = privKeyEntry.getCertificate().getPublicKey();

        params.setKey(symmetricKey.getEncoded());
        // execute symmetric encryption
        this.symmetricEncryption(params);
        // encrypt the key with the public key
        Cipher cipher = Cipher.getInstance(PKI_CIPHER_ALG);
        cipher.init(Cipher.WRAP_MODE, pubKey);
        byte[] wrappedKey = cipher.wrap(symmetricKey);
        LOGGER.log(Level.INFO, "Wrapped key: {0}", Base64.getEncoder().encodeToString(wrappedKey));
        params.setKey(wrappedKey);
        
        return params;
    }
    

    private EncryptionTestParameters asymmetricDecryption(EncryptionTestParameters params) throws Exception {
        // generate random key
        byte[] encryptedKey = params.getKey();
         
        KeyStore.PrivateKeyEntry privKeyEntry = (KeyStore.PrivateKeyEntry) this.getKeystoreEntry();
        PrivateKey privKey = privKeyEntry.getPrivateKey();
        
        // decrypt the key with the private key
        Cipher cipher = Cipher.getInstance(PKI_CIPHER_ALG);
        cipher.init(Cipher.UNWRAP_MODE, privKey);
        Key symmetricKey = cipher.unwrap(encryptedKey, SYMMETRIC_KEY_ALG, Cipher.SECRET_KEY);
        params.setKey(symmetricKey.getEncoded());
        
        // execute symmetric decryption
        this.symmetricDecryption(params);
        
        return params;
    }    
    
    private KeyStore.Entry getKeystoreEntry() throws 
            KeyStoreException, NoSuchAlgorithmException, 
            UnrecoverableEntryException, IOException, CertificateException  {
        KeyStore keystore = KeyStore.getInstance("JKS");
        try (InputStream in = new FileInputStream(KEYSTORE_FILE)) {
            keystore.load(in, KEYSTORE_FILE_PASSWORD.toCharArray());
            return keystore.getEntry(KEYSTORE_ALIAS, new  KeyStore.PasswordProtection(KEYSTORE_KEY_PASSWORD.toCharArray()));
        }
    }

    /**
     * this implementation of deep equals 
     * provides constant time compare
     * for hash
     *
     * @param t1
     * @param t2
     * @return
     */
    private boolean compareTags(byte[] t1, byte[] t2) {
        byte result = 0;
        if (t1.length != t2.length) {
            return false;
        }
        for (int i = 0; i < t1.length; i++) {
            result |= t1[i] ^ t2[i];
        }
        return (result==0);
    }

}

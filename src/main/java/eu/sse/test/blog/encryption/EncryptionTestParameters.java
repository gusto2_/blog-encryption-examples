
package eu.sse.test.blog.encryption;

/**
 *
 * @author Gabriel
 */
public class EncryptionTestParameters {
    
    private byte[] plaintext;
    private byte[] ciphertext;
    private byte[] key;
    private byte[] iv;
    private byte[] salt;
    private byte[] mac;

    public byte[] getPlaintext() {
        return plaintext;
    }

    public void setPlaintext(byte[] plaintext) {
        this.plaintext = plaintext;
    }

    public byte[] getCiphertext() {
        return ciphertext;
    }

    public void setCiphertext(byte[] ciphertext) {
        this.ciphertext = ciphertext;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public byte[] getIv() {
        return iv;
    }

    public void setIv(byte[] iv) {
        this.iv = iv;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public byte[] getMac() {
        return mac;
    }

    public void setMac(byte[] mac) {
        this.mac = mac;
    }
    
    
    
}

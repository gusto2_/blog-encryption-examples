Encryption reference project
======

# Scope #

This project provides **reference implementation for encryption decryption 
in Java**. Only basic Java 8 features are used with no extra libraries. 

The goal is to have a reference implementation how to encrypt / decrypt 
data properly using basic JRE tools, at this time neglecting encryption speed, hashing speed, etc.. 
Effectively this approach should be valid in all programming languages or tools.


Lowest feasible settings are used (AES-128, SHA-256, 1024-bit RSA, RSA-PKCS1)
feasible for users with Java 7 and without Unlimited Strength JCE Policy.

For simplification:

 * only constant string is used (no streaming, updates, ..)
 * only block ciphers are used
 * inputs and outputs are stored only in a Java class, 
   so we don't deal with file handling, etc..


Reference use cases in this project:

 * simple symmetric encryption and decryption
 * password symmetric encryption and decryption
 * asymmetric encryption

There are more ways and use cases, but this is typically used most often I see.
What is not implemented (maybe it will come later)

 * stream ciphers
 * CTR mode (allowing partial decryption)

# Rules to follow 

  * don't invent your own crypto **!!!!!!!**
  * password is not key
  * cut one of your fingers for each time you reuse a nonce
  * treat unauthenticated ciphertext as nuclear waste


# Encryption use cases 


## Simple symmetric encryption

```
    test.symmetricEncryption(encryptionParams);
    test.symmetricDecryption(encryptionParams);


```

There is a list of ciphers required to be supported by any Java implementation,
see the [Cipher javadoc](https://docs.oracle.com/javase/8/docs/api/javax/crypto/Cipher.html)

One of the ciphers is chosen: AES-128 CBC mode (AES/CBC/PKCS5Padding) as is guaranteed
to be available and includes the [padding](https://en.wikipedia.org/wiki/Padding_(cryptography)) (we don't have to deal with it separately)

The test application will print a whole list of available services (ciphers, modes, ..)
however they are not guaranteed to be available on different versions, systems 
or environments.

A big list of other features (cipher, keys, hashes, ..) is available 
with the [Bouncy Castle](https://www.bouncycastle.org/) project.


Lessons to be learned:

 * use random IV (initialization vector / nonce)
 * use authenticated encryption
 * key must have fixed size depending on cipher used
 * data are encrypted in blocks (with block ciphers),
   for simplification a cipher with implicit [PKCS #5 padding](https://tools.ietf.org/html/rfc8018) is used,
   otherwise we will have to implement the padding by our own.

Key size:

 * [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) - 128, 192 or 256 bits
 * [3DES](https://en.wikipedia.org/wiki/Triple_DES) (DESede) - 168 or 112 bits (according to [OpenSSL](https://www.openssl.org/blog/blog/2016/08/24/sweet32/), 
   effective key strength is 80 bits, so 3DES is considered as a weak cipher)
 * [DES](https://en.wikipedia.org/wiki/Data_Encryption_Standard) - 56 bit (too short for current times, use only for backward compatibility)
 * [Blowfish](https://en.wikipedia.org/wiki/Blowfish_(cipher)) - 32–448 bits
 * [ARCFOUR](https://en.wikipedia.org/wiki/RC4) - 40–2048 bits
 * [RC2](https://en.wikipedia.org/wiki/RC2) - 8–1024 bits



## Password symmetric encryption

```
            // password encryption
            encryptionParams = new EncryptionTestParameters();
            encryptionParams.setPlaintext(PLAINTEXT.getBytes("UTF-8"));
            encryptionParams = test.symmetricPasswordEncryption(encryptionParams, TEXT_PASSWORD);
            test.symmetricPasswordDecryption(encryptionParams, TEXT_PASSWORD);
            
            // password encryption with direct PBE cipher
            // PBE cipher implicitly provides CBC with PKCS5Padding (to be confirmed)
            encryptionParams = new EncryptionTestParameters();
            encryptionParams.setPlaintext(PLAINTEXT.getBytes("UTF-8"));
            encryptionParams = test.symmetricPasswordEncryption2(encryptionParams, TEXT_PASSWORD);
            test.symmetricPasswordDecryption2(encryptionParams, TEXT_PASSWORD);
```


Password based encryption (PBE) is the same as previous symmetric encryption,
just key key is based (derived) from passwords.

The issues with user-entered (and remembered) passwords are

 * passwords tend to have low entropy (are not long and random enough)
   and often are feasible for dictionary or brute-force attack
 * password length does not match required key length

Therefore the encryption keys are derived from the password using
the [PBKDF (Password-Based Key Derivation Function)](https://en.wikipedia.org/wiki/PBKDF2).


PBKDF will output a key with required length using salt and iteration count parameters.
The password salt (nonce) will make the output "randomized" (so even when 
reusing the password the key will be different). The iteration count should make
brute-forcing and dictionary attack less feasible.



## Asymmetric encryption

Asymmetric encryption is based on fact that for encryption and decryption there 
are different keys used. In this example we will use RSA keypair stored in the JKS keystore

Asymmetric encryption is feasible for encryption of data much smaller than the 
asymmetric key length, therefore for encrypting larger chunk of data only 
a random symmetric key is encrypted. The symmetric key is used to encrypt the data.

```
    // rsa encryption
    encryptionParams = new EncryptionTestParameters();
    encryptionParams.setPlaintext(PLAINTEXT.getBytes("UTF-8"));
    test.asymmetricEncryption(encryptionParams);
    test.asymmetricDeryption(encryptionParams);
```


# Resources

Other useful resources over cryptography:

 * [Block cipher mode of operation](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation)
 * [Schneier on Security](https://www.schneier.com/)
 * [StackOverflow Cryptography](https://crypto.stackexchange.com/) 
 * [Coursera](https://www.coursera.org/)
